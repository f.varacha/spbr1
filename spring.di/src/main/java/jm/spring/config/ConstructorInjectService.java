package jm.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// tag::ConstructorInjectService[]
@Component
public class ConstructorInjectService {

    private final CarDao carDao; // <1>

    @Autowired // <2>
    public ConstructorInjectService(CarDao carDao) { // <3>
        this.carDao = carDao;
    }
// end::ConstructorInjectService[]

    /**
     * Getter for testing purpose only.
     */
    CarDao getCarDao() {
        return carDao;
    }
}
