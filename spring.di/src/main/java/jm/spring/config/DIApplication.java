package jm.spring.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DIApplication {

	public static void main(String[] args) {
		SpringApplication.run(DIApplication.class, args);
	}

// tag::vipCarDao[]
	@Bean
	public CarDao vipCarDao() {
		return new CarDao();
	}
// end::vipCarDao[]

}
