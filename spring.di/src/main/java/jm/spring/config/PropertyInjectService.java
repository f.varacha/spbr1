package jm.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// tag::PropertyInjectService[]
@Component
public class PropertyInjectService {

    @Autowired // <1>
    private CarDao carDao; // <2>
// end::PropertyInjectService[]

    /**
     * Getter for testing purpose only.
     */
    CarDao getCarDao() {
        return carDao;
    }
}
