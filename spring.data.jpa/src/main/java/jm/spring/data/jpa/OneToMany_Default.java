package jm.spring.data.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;
import java.util.UUID;

public class OneToMany_Default {

    //tag::entities[]
    @Entity
    public class Circus {
        @Id
        private UUID id;

        @OneToMany
        public Collection<Tiger> tigers;
    }

    @Entity
    public class Tiger {
        @Id
        private UUID id;
    }
    //end::entities[]
}
