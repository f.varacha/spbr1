package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;

public class ManyToMany_Table {

    //tag::entities[]
    @Entity
    public class Employer {
        @Id
        private UUID id;

        @ManyToMany(
                cascade = {CascadeType.PERSIST, CascadeType.MERGE}
        )
        @JoinTable(
                name = "ManyToMany_Table$employer_employee",
                joinColumns = @JoinColumn(name = "EMPER_ID"),
                inverseJoinColumns = @JoinColumn(name = "EMPEE_ID")
        )
        public Collection<Employee> employees;
    }

    @Entity
    public class Employee {
        @Id
        private UUID id;

        @ManyToMany(
                cascade = {CascadeType.PERSIST, CascadeType.MERGE},
                mappedBy = "employees"
        )
        public Collection<Employer> employers;
    }
    //end::entities[]
}
