package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.UUID;

public class ManyToOne_Interface {

    //tag::entities[]
    @Entity
    public class Book {
        @Id
        private UUID id;

        @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = Person.class)
        @JoinColumn(name="author_ID")
        public Author author;
        
    }

    public interface Author {
    }

    @Entity
    public class Person implements Author {
        @Id
        private UUID id;
    }
    //end::entities[]
}
