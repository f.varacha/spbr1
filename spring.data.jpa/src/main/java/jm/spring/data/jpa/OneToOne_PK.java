package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.UUID;

public class OneToOne_PK {

    //tag::entities[]
    @Entity
    public class Body {
        @Id
        private UUID id;

        @OneToOne(cascade = CascadeType.ALL)
        @PrimaryKeyJoinColumn // <1>
        private Heart heart;
    }

    @Entity
    public class Heart {
        @Id
        private UUID id;
    }
    //end::entities[]
}
