package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.UUID;

public class ManyToOne_FK {

    //tag::entities[]
    @Entity
    public class Flight {
        @Id
        private UUID id;

        @ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
        @JoinColumn(name="COMP_ID")
        public Company company;

    }

    @Entity
    public class Company {
        @Id
        private UUID id;
    }
    //end::entities[]
}
