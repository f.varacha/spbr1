package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;

public class OneToMany_Bidirectional {

    //tag::entities[]
    @Entity
    public class Troop {
        @Id
        private UUID id;

        @OneToMany(mappedBy = "troop")
        public Collection<Soldier> soldiers;
    }

    @Entity
    public class Soldier {
        @Id
        private UUID id;

        @ManyToOne
        @JoinColumn(name = "troop_fk")
        public Troop troop;
    }
    //end::entities[]
}
