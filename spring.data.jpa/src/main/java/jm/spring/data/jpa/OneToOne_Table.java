package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.UUID;

public class OneToOne_Table {

    //tag::entities[]
    @Entity
    public class Car {
        @Id
        private UUID id;

        @OneToOne(cascade = CascadeType.ALL)
        @JoinTable(name = "one_to_one_table$car_motor",
                joinColumns = @JoinColumn(name = "car_fk"),
                inverseJoinColumns = @JoinColumn(name = "motor_fk")
        )
        public Motor motor;
    }

    @Entity
    public class Motor {
        @Id
        private UUID id;

        @OneToOne(mappedBy = "motor")
        public Car car;
    }
    //end::entities[]
}
