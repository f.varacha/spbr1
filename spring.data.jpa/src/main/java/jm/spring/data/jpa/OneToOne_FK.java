package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.UUID;

public class OneToOne_FK {

    //tag::entities[]
    @Entity
    public class Customer {
        @Id
        private UUID id;

        @OneToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "passport_fk")
        public Passport passport;
    }

    @Entity
    public class Passport {
        @Id
        private UUID id;

        @OneToOne(mappedBy = "passport")
        public Customer owner;
    }
    //end::entities[]
}
