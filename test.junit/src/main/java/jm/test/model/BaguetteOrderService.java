package jm.test.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BaguetteOrderService implements OrderService {

    private DeliveryService deliveryService;
    private PriceService priceService;

    public BaguetteOrderService(DeliveryService deliveryService, PriceService priceService) {
        this.deliveryService = deliveryService;
        this.priceService = priceService;
    }

    @Override
    public Order createOrder(User user, Basket basket) {
        Order order = new Order();
        order.setUser(user);
        order.setCategory(establishCategory(user, basket));
        order.setItems(basketContentToItems(basket));
        order.setDeliveryDay(deliveryService.calculateDeliveryDay(order.getCategory(), order.getItems()));
        if (order.getCategory() == Order.Category.PRIORITY) {
            order.setFinalPrise(priceService.privilegePrice(user, order.getItems()));
        } else {
            order.setFinalPrise(priceService.regularPrice(order.getItems()));
        }
        return order;
    }

    private List<Order.Item> basketContentToItems(Basket basket) {
        return basket.stream()
                .map(e -> new Order.Item(e.getKey(), e.getValue(), this.itemPrise(e)))
                .collect(Collectors.toList());
    }

    private BigDecimal itemPrise(Map.Entry<Product, Integer> entry) {
        Product product = entry.getKey();
        BigDecimal amount = BigDecimal.valueOf(entry.getValue());
        return product.getPrise().multiply(amount);
    }

    @Override
    public Order.Category establishCategory(User user, Basket basket) {
        if (user.getPrepaidUntil().isAfter(LocalDate.now()))
            return Order.Category.PRIORITY;

        long workingPrise = basket.stream()
                .mapToLong(e -> e.getKey().getPrise().longValue() * e.getValue())
                .sum();
        if (workingPrise >= 1000l) {
            return Order.Category.PRIORITY;
        }

        return Order.Category.REGULAR;
    }

}
