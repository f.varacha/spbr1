package jm.test.model;

import java.time.LocalDate;
import java.util.List;

/**
 * Rozhraní pr
 */
public interface DeliveryService {

    /**
     * Vypočet data doručení podle dostupností ve skladne.
     * @param category categorie objednávky
     * @param items seznam položek
     * @return
     */
    LocalDate calculateDeliveryDay(Order.Category category, List<Order.Item> items);
}
