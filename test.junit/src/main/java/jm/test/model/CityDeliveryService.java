package jm.test.model;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class CityDeliveryService implements DeliveryService {

    private WarehouseService warehouseService;

    private int priorityLimit = 32;

    public CityDeliveryService(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    public int getPriorityLimit() {
        return priorityLimit;
    }

    public void setPriorityLimit(int priorityLimit) {
        this.priorityLimit = priorityLimit;
    }

    @Override
    public LocalDate calculateDeliveryDay(Order.Category category, List<Order.Item> items) {
        int stockLimit = category == Order.Category.PRIORITY ? 0 : priorityLimit;
        return items.stream()
                .filter(item -> item.amount + stockLimit < warehouseService.getAmountOfStoredProduct(item.product))
                .map(item -> warehouseService.getNextProductStorageDay(item.product))
                .sorted(Comparator.reverseOrder())
                .findFirst()
                .orElse(LocalDate.now().plusDays(1));
    }



}
