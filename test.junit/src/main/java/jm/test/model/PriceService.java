package jm.test.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * Tato třída počítá finální cenu nakupu.
 */
public interface PriceService {

    /**
     * Cena pro obyčejnou objednávku
     * @param items seznam položek
     * @return výsledná cena
     */
    BigDecimal regularPrice(List<Order.Item> items);

    /**
     * Vupočet zvýhodněnné ceny
     * @param user informace o uživateli
     * @param items senzma položek
     * @return výsledná cena
     */
    BigDecimal privilegePrice(User user, List<Order.Item> items);

}
