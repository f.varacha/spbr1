package jm.test.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Vytvořená objednávka.
 */
public class Order {

    /**
     * Kategorie, do které objednávka spadá.
     */
    public enum Category {
        /** Klasická objednávka */
        REGULAR,
        /** Objednávka s přednodtním vyřízením */
        PRIORITY
    }

    public static class Item {

        public final Product product;
        public final Integer amount;
        public final BigDecimal itemPrise;

        public Item(Product product, Integer amount, BigDecimal itemPrise) {
            this.product = product;
            this.amount = amount;
            this.itemPrise = itemPrise;
        }
    }

    private User user;

    private Category category;

    private List<Item> items;

    private LocalDate deliveryDay;

    private BigDecimal finalPrise;

    /**
     * Uživatel poro kterého je objednávka vygenerována.
     */
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Kategorie do které objednavka spadá.
     */
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * Seznam položek objednávky.
     */
    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * Datum doručení objednávky
     */
    public LocalDate getDeliveryDay() {
        return deliveryDay;
    }

    public void setDeliveryDay(LocalDate deliveryDay) {
        this.deliveryDay = deliveryDay;
    }

    /**
     * Finální cena objednáky.
     * Se započítanou slevou a cenou za dopravu.
     * @return
     */
    public BigDecimal getFinalPrise() {
        return finalPrise;
    }

    public void setFinalPrise(BigDecimal finalPrise) {
        this.finalPrise = finalPrise;
    }
}
