package jm.test.model;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class InMemoryWarehouseService implements WarehouseService {

    private final Map<Product, Integer> amountMap;
    private final Map<Product, LocalDate> storageDayMap;

    public InMemoryWarehouseService() {
        this.amountMap = new HashMap<>();
        this.storageDayMap = new HashMap<>();
    }

    public void addRecord(Product product, Integer amount, LocalDate nextStorageDay) {
        amountMap.put(product, amount);
        storageDayMap.put(product, nextStorageDay);
    }

    @Override
    public int getAmountOfStoredProduct(Product product) {
        return amountMap.getOrDefault(product, 0);
    }

    @Override
    public LocalDate getNextProductStorageDay(Product product) {
        return storageDayMap.get(product);
    }
}
