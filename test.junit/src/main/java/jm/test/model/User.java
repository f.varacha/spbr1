package jm.test.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 * Třída reprezentujícího uživatele.
 */
public class User {

    private final String login;

    private Boolean children;

    private LocalDate prepaidUntil;

    public User(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public Boolean getChildren() {
        return children;
    }

    public void setChildren(Boolean children) {
        this.children = children;
    }

    public LocalDate getPrepaidUntil() {
        return prepaidUntil;
    }

    public void setPrepaidUntil(LocalDate prepaidUntil) {
        this.prepaidUntil = prepaidUntil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", children=" + children +
                ", prepaidUntil=" + prepaidUntil +
                '}';
    }
}
