package jm.test.model;

/**
 * Nekontro
 */
public class ShopException extends RuntimeException {

    public ShopException(String message) {
        super(message);
    }
}
