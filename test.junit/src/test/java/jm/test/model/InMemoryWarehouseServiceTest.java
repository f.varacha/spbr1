package jm.test.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryWarehouseServiceTest {

    private InMemoryWarehouseService inMemoryWarehouseService;

    @BeforeEach
    public void initInMemoryWarehouseService() {
        this.inMemoryWarehouseService = new InMemoryWarehouseService();
    }

    @Test
    void getAmountOfStoredProduct() {
        Product product = new Product("Test 1", BigDecimal.TEN);
        inMemoryWarehouseService.addRecord(product, 10, null);

        assertEquals(10, inMemoryWarehouseService.getAmountOfStoredProduct(product));
    }

    @Test
    void getAmountOfStoredProduct_nonExistsProduct() {
        Product product = new Product("Test 0", BigDecimal.TEN);

        assertEquals(0, inMemoryWarehouseService.getAmountOfStoredProduct(product));
    }

    @Test
    void getNextProductStorageDay() {
        Product product = new Product("Test 1", BigDecimal.TEN);
        inMemoryWarehouseService.addRecord(product, 10, LocalDate.of(1,1,2000));

        assertEquals(LocalDate.of(1,1,2000), inMemoryWarehouseService.getNextProductStorageDay(product));


    }
}