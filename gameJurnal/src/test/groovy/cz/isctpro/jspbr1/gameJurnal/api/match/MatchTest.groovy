package cz.isctpro.jspbr1.gameJurnal.api.match

import cz.isctpro.jspbr1.gameJurnal.api.match.participant.Participant
import spock.lang.Specification
import spock.lang.Unroll

class MatchTest extends Specification {

    def "Demo groovy implicit builder test."() {
        when:
        def match = new Match(number: 1)

        then:
        match.toString() == "Match(numger=1)"
    }

    @Unroll
    def "Number test: number = #n"() {
        given:
        def match = new Match(
                number: n,
                participants: [new Participant(score: s)])
        expect:
        match.number == n
        match.participants.size() == size

        where:
        n | s | size
        1 | 10 | 1
        3 | 50 | 1
    }
}
