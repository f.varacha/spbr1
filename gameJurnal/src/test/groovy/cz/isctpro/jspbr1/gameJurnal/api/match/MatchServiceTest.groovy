package cz.isctpro.jspbr1.gameJurnal.api.match

import cz.isctpro.jspbr1.gameJurnal.api.match.participant.Participant
import cz.isctpro.jspbr1.gameJurnal.api.match.participant.ParticipantService
import spock.lang.Ignore
import spock.lang.Specification

class MatchServiceTest extends Specification {

    MatchService matchService;
    ParticipantService participantService;

    void setup() {
        matchService = new MatchService();
        participantService = matchService.participantService = Mock(ParticipantService)
    }

    def "Create new Match"() {
        given:
        participantService.create(_) >>> [
                new Participant(player: "First"),
                new Participant(player: "Second")
        ]
        Map[] p = [
                ["name": "Papa", "score": 50],
                ["name": "Honaza", "score": 55],
        ];

        when:
        def result = matchService.createMatch("Pexeso", null, p)

        then:
        result.block() == null

    }


    @Ignore
    def plusTest() {
        given:
        MatchService matchService = new MatchService();
        matchService.setA(10);

        when:
        int result = matchService.plus(5);

        then:
        matchService.a == 15
        result == 15
    }
}
