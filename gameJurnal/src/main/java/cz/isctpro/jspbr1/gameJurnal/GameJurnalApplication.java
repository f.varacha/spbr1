package cz.isctpro.jspbr1.gameJurnal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameJurnalApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameJurnalApplication.class, args);
	}

}
