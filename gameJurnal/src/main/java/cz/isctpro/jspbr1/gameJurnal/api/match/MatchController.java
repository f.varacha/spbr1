package cz.isctpro.jspbr1.gameJurnal.api.match;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.security.Principal;
import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = "api/match",
        produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "/api/match")
class MatchController {

    @Autowired
    private MatchService matchService;

    @Autowired
    private MatchRepository matchRepository;

    @GetMapping
    public Flux<Match> getMatch(Principal principal) {
        System.out.println(principal);
        return Flux.fromIterable(matchRepository.findAllWithPatricipants());
    }

    @GetMapping("/scores")
    public Flux<Score> getScores() {
        return Flux.fromIterable(matchRepository.findScores());
    }

    @GetMapping("/{id}")
    public Mono<Match> getMatch(
            @PathVariable int id
    ) {
        var match = new Match();
        return Mono.just(match);
    }

    @Data
    @ApiModel(description = "To je objekpto pro založení záznamu ...")
    static class PostMatchRequest {
        @Pattern(
                regexp = "[a-zA-z0-9 ]{3,128}"
        )
        @ApiModelProperty(
                example = "Pexeso",
                notes = "Jméno hry ve formátu ..."
        )
        String name;

        @Past
        LocalDate date;

        @NotNull
        Map[] participant;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Match> postMatch(
            @RequestBody @Validated PostMatchRequest request
    ) {
        return matchService
                .createMatch(
                        request.name,
                        request.date,
                        request.participant);
    }
}
