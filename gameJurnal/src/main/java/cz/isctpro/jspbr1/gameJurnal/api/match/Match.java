package cz.isctpro.jspbr1.gameJurnal.api.match;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.isctpro.jspbr1.gameJurnal.api.match.participant.Participant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "matches")
public class Match {

    @Id
    @EqualsAndHashCode.Exclude
    private UUID id = UUID.randomUUID();

    private int number;

    private String name;

    private LocalDate date;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="match_id")
    private Collection<Participant> participants;
}
