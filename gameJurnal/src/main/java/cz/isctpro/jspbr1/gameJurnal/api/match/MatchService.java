package cz.isctpro.jspbr1.gameJurnal.api.match;

import cz.isctpro.jspbr1.gameJurnal.api.match.participant.ParticipantService;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MatchService {

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private MatchRepository matchRepository;

    @Value("#{new java.util.concurrent.atomic.AtomicInteger(${match.startNumber})}")
    private AtomicInteger nextNumber = new AtomicInteger();

    /**
     * This method create new Match.
     *
     * @param name
     * @return Mono with created Match
     */
    @Transactional
    public Mono<Match> createMatch(String name, LocalDate date, Map[] participants) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        System.out.println(securityContext.getAuthentication());

        var match = new Match();
        match.setName(name);
        match.setNumber(nextNumber.incrementAndGet());
        match.setDate(date != null ? date : LocalDate.now());
        match.setParticipants(
                Arrays.stream(participants)
                        .map(participantService::create)
                        .collect(Collectors.toList())
        );
        var saved = matchRepository.save(match);
        log.info("New match created {}.", saved);
        return Mono.just(saved);
    }

}
