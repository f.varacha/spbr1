package cz.isctpro.jspbr1.gameJurnal.api.player;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.UUID;

@Data
public class Player {

    @JsonIgnore
    private UUID id;

    private String name;
}
