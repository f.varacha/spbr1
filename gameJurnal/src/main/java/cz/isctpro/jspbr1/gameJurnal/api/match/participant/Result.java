package cz.isctpro.jspbr1.gameJurnal.api.match.participant;

public enum Result {

    WIN, LOSE;
}
