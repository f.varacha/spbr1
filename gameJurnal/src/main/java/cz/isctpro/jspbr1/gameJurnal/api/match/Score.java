package cz.isctpro.jspbr1.gameJurnal.api.match;

import lombok.Value;

@Value
public class Score {
    private String player;
    private long score;
}
