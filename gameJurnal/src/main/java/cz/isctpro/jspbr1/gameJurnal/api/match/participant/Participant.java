package cz.isctpro.jspbr1.gameJurnal.api.match.participant;

import cz.isctpro.jspbr1.gameJurnal.api.player.Player;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@Table(name= "participants")
public class Participant {

    @Id
    @EqualsAndHashCode.Exclude
    private UUID id = UUID.randomUUID();

    private String player;
    private Integer score;
    private Result result;

}
