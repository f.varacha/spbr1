package cz.isctpro.jspbr1.gameJurnal.api.match.participant;

import cz.isctpro.jspbr1.gameJurnal.api.match.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ParticipantService {

    public Participant create(Map data) {
        var participant = new Participant();
        participant.setPlayer(data.getOrDefault("name", "NONE").toString());
        participant.setScore(Integer.parseInt(data.getOrDefault("score", -1).toString()));
        return participant;
    }
}
