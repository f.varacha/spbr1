package cz.isctpro.jspbr1.gameJurnal.api.match;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface MatchRepository extends CrudRepository<Match, UUID> {

    @Query("select m from Match m join fetch m.participants")
    Iterable<Match> findAllWithPatricipants();

//    @Query(nativeQuery = true,
//    value = "select PLAYER, sum(SCORE) from PARTICIPANTS group by PLAYER")
    @Query("select new cz.isctpro.jspbr1.gameJurnal.api.match.Score(p.player, sum(p.score)) from Participant p group by p.player")
    Iterable<Score> findScores();
}
