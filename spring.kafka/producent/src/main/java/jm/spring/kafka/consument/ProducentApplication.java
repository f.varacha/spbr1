package jm.spring.kafka.consument;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducentApplication.class, args);
	}

}
