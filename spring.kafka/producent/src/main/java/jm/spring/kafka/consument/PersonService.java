package jm.spring.kafka.consument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PersonService {

    // @Value("${person.default-level}")
    private int defaultLevel = 1;

    // @Value("${person.kafka-topic}")
    private String springKafkaTopic = "spring.kafka";

    @Autowired
    private KafkaTemplate<UUID, Person> kafkaTemplate;

    public Person create(String name) {
        Person person = new Person(name, defaultLevel);
        kafkaTemplate.send(springKafkaTopic, person.getId(), person);
        return person;
    }
}
