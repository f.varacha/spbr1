package jm.spring.kafka.consument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class PersonShell {

    @Autowired
    private PersonService personService;

    @ShellMethod("Create new person by name")
    public String createPerson(
            @ShellOption String name
    ) {
        return personService.create(name).toString();
    }
}
