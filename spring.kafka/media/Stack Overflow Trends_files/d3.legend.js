﻿// From http://bl.ocks.org/ZJONSSON/3918369
// d3.legend.js
// (C) 2012 ziggy.jonsson.nyc@gmail.com
// MIT licence

(function () {
    d3.legend = function (g, title) {
        g.each(function () {
            var legendItemPadding = { top: 0 };

            var g = d3.select(this),
                items = {},
                svg = d3.select(g.property("nearestViewportElement")),
                legendPadding = g.attr("data-style-padding") || 5,
                legendBox = g.selectAll(".legend-box").data([true]);

            if (title) {
                var legendTitle = g
                    .append("text")
                    .attr("class", "legend-title")
                    .text("Tag")
                    .style("font-weight", "bold");

                legendItemPadding.top = 20;
            }

            var legendItems = g
                .selectAll(".legend-items")
                .data([true])
                .enter()
                .append("g")
                .attr("class", "legend-items")
                .attr("transform", "translate(0," + legendItemPadding.top + ")");

            legendBox
                .enter()
                .append("rect")
                .classed("legend-box", true);

            svg.selectAll("[data-legend]").each(function () {
                var self = d3.select(this);
                var dataLegendColor = self.attr("data-legend-color");

                items[self.attr("data-legend")] = {
                    pos: self.attr("data-legend-pos") || this.getBBox().y,
                    color: dataLegendColor !== undefined && dataLegendColor !== null ? dataLegendColor : self.style("fill") !== 'none' ? self.style("fill") : self.style("stroke")
                };
            });

            items = d3.entries(items).sort(function (a, b) { return a.value.pos - b.value.pos; });

            var mouseOver = function (d) {
                d3.select(this)
                    .style("font-weight", "bold");

                var selectlegend = $(".lines path.line").each(function (index, element) {
                    if ($(element).data("legend") != d.key) {
                        $(element).css("opacity", .2);
                    } else {
                        $(element).css("opacity", 1);
                        $(element)
                            .css("stroke-width", '4px');
                    }
                });
            }
            var mouseOut = function (d) {
                d3.select(this)
                    .style("font-weight", "normal");

                d3.selectAll('.lines path.line')
                    .style("opacity", 1)
                    .style("stroke-width", '1px');

            };

            legendItems
                .selectAll()
                .data(items)
                .enter()
                .append("text")
                .attr("y", function (d, i) { return i + "em"; })
                .attr("x", "1em")
                .text(function (d) { return d.key; })
                .on("mouseover", mouseOver)
                .on("mouseout", mouseOut);

            legendItems
                .selectAll()
                .data(items)
                .enter()
                .append("circle")
                .attr("cy", function (d, i) { return i - 0.25 + "em"; })
                .attr("cx", 0)
                .attr("r", "0.4em")
                .style("fill", function (d) { return d.value.color; })
                .on("mouseover", mouseOver)
                .on("mouseout", mouseOut);

            // Reposition and resize the box
            var legendBoxBBox = this.getBBox();

            legendBox.attr("x", legendBoxBBox.x - legendPadding)
                .attr("y", legendBoxBBox.y - legendPadding)
                .attr("height", legendBoxBBox.height + 2 * legendPadding)
                .attr("width", legendBoxBBox.width + 2 * legendPadding);
        });

        return g;
    };
})();