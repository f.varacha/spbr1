/// <reference path="../typings/globals/jquery/index.d.ts"/>;
/// <reference path="../typings/globals/selectize/index.d.ts"/>;
/// <reference path="../typings/modules/d3/index.d.ts"/>;
/// <reference path="../typings/modules/d3-selection/index.d.ts"/>;
/// <reference path="../typings/modules/d3-axis/index.d.ts"/>;
/// <reference path="../typings/modules/d3-legend/index.d.ts"/>;
var Insights;
(function (Insights) {
    var TagTrend = (function () {
        function TagTrend() {
            this.values = new Array();
        }
        return TagTrend;
    }());
    var TrendValue = (function () {
        function TrendValue(yearMonth, percent) {
            this.yearMonth = yearMonth;
            this.percent = percent;
        }
        return TrendValue;
    }());
    var Trends = (function () {
        function Trends() {
            this.yearMonths = [];
            this.tags = [];
            var that = this;
            this.$tags = $("#tags");
            $(".tag-preset").click(function (e) {
                that.presetClick.call(that, this);
                e.preventDefault();
                return false;
            });
            this.loadData();
        }
        Trends.prototype.presetClick = function (element) {
            var tagData = $(element).data("tags");
            this.$tags.val(tagData);
            this.refreshTags();
            var selectize = $("#tags")[0].selectize;
            selectize.clear(true);
            for (var i = 0; i < this.selectedTags.length; i++) {
                selectize.addItem(this.selectedTags[i], true);
            }
        };
        Trends.prototype.getParameterByName = function (name, url) {
            if (!url)
                url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
            if (!results)
                return null;
            if (!results[2])
                return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        };
        Trends.prototype.updateQueryStringParameter = function (uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        };
        Trends.prototype.parseYears = function () {
            for (var i = 0; i < this.model.Year.length; i++) {
                var year = this.model.Year[i];
                var month = this.model.Month[i];
                var yearMonth = year + '-' + month;
                this.yearMonths.push(this.timeParse(yearMonth));
            }
        };
        ;
        Trends.prototype.refreshTags = function () {
            var joinedTags = this.$tags.val();
            if (joinedTags) {
                var tags = this.split(joinedTags);
                this.selectedTags = tags;
            }
            else {
                this.selectedTags = [];
            }
            var newUrl = window.location.pathname + this.updateQueryStringParameter(window.location.search, "tags", encodeURIComponent(joinedTags));
            history.pushState(null, "", newUrl);
            this.filterData();
            this.renderChart();
            this.updateDownloadLink();
        };
        ;
        Trends.prototype.loadData = function () {
            var that = this;
            var jqxhr = $.get({ url: trendsDataFileUrl, cache: true})
                .done(function (data) {
                that.model = data;
                that.parseYears();
                that.parseTags();
                var queryTagsString = that.getParameterByName('tags', window.location.search);
                if (queryTagsString) {
                    var queryTags = queryTagsString.split(',');
                    that.selectedTags = queryTags;
                }
                else {
                    that.selectedTags = ['r', 'statistics'];
                }
                that.$tags.val(that.selectedTags.join(','));
                var options = that.tags.map(function (t) { return { item: t }; });
                that.$tags.selectize({
                    plugins: ['remove_button'],
                    delimiter: ',',
                    persist: false,
                    options: options,
                    items: this.selectedTags,
                    labelField: "item",
                    valueField: "item",
                    searchField: ['item'],
                    allowEmptyOption: false,
                    maxItems: 15,
                    maxOptions: 100,
                    onChange: function () {
                        var a = that;
                        a.refreshTags();
                    }
                });
                that.refreshTags();
            });
        };
        Trends.prototype.split = function (value) {
            return value.split(/,/);
        };
        Trends.prototype.extractLast = function (term) {
            return this.split(term).pop();
        };
        Trends.prototype.parseTags = function () {
            for (var tagName in this.model.TagPercents) {
                this.tags.push(tagName);
            }
        };
        Trends.prototype.verifyTags = function () {
            for (var i = 0; i < this.selectedTags.length; i++) {
                var tagName = this.selectedTags[i];
                var tagPercent = this.model.TagPercents[tagName];
                if (!tagPercent) {
                    this.selectedTags.splice(i, 1);
                    i--;
                    continue;
                }
            }
            this.updateTagInput();
        };
        Trends.prototype.updateTagInput = function () {
            this.$tags.val(this.selectedTags.join(" "));
        };
        Trends.prototype.timeParse = function (value) {
            return d3.timeParse("%Y-%m")(value);
        };
        Trends.prototype.filterData = function () {
            this.verifyTags();
            this.selectedTagTrends = [];
            for (var i = 0; i < this.selectedTags.length; i++) {
                var tagTrend = new TagTrend();
                tagTrend.tagName = this.selectedTags[i];
                var tagData = this.model.TagPercents[tagTrend.tagName];
                for (var j = 1; j < tagData.length; j++) {
                    var year = this.model.Year[j];
                    var month = this.model.Month[j];
                    var value = tagData[j];
                    var yearMonth = this.timeParse(year + '-' + month);
                    tagTrend.values.push(new TrendValue(yearMonth, value / 100));
                }
                this.selectedTagTrends.push(tagTrend);
            }
        };
        Trends.prototype.renderChart = function () {
            var data = this.selectedTagTrends;
            var margin = { top: 20, right: 150, bottom: 80, left: 100 }, width = 660 - margin.left - margin.right, height = 400 - margin.top - margin.bottom, gridBuffer = { x: 20, y: 20 };
            var svg = d3.select("#trend-chart");
            svg.selectAll("*").remove();
            svg
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g");
            var chartArea = svg
                .append("g")
                .attr("class", "chart-area")
                .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
            // X axis
            var x = d3.scaleTime()
                .domain(d3.extent(this.yearMonths, function (d) {
                return d;
            }))
                .range([0, width]);
            // Draw x axis
            chartArea
                .append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));
            // Draw x axis gridlines
            chartArea
                .append("g")
                .attr("class", "grid-lines")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x)
                .tickSize(-height))
                .selectAll("text").remove();
            // Draw x axis label
            chartArea
                .append("text")
                .attr("x", width / 2)
                .attr("y", height + margin.top + margin.bottom / 2)
                .attr("class", "axis-label x-axis-label")
                .attr("style", "text-anchor: middle; font-weight: bold;")
                .text("Year");
            // Y axis
            var minY = 0;
            var maxY = d3.max(data, function (d) { return d3.max(d.values, function (d) { return d.percent; }); });
            if (maxY > 0.01)
                maxY = Math.ceil(maxY * 100) / 100;
            else
                maxY = Math.ceil(maxY * 1000) / 1000;
            var y = d3.scaleLinear()
                .domain([minY, maxY])
                .range([height, 0]);
            // Draw y axis
            chartArea
                .append("g")
                .call(d3.axisLeft(y)
                .ticks(11)
                .tickFormat(d3.format(".2%")));
            // Draw y axis gridlines
            chartArea
                .append("g")
                .attr("class", "grid-lines")
                .call(d3.axisLeft(y)
                .tickSize(-width))
                .selectAll("text").remove();
            // Draw y axis label
            svg
                .append("g")
                .attr("transform", "translate(20, " + height / 2 + ")")
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("x", margin.left / 2 - 100)
                .attr("y", 0)
                .attr("dy", "1em")
                .attr("class", "axis-label y-axis-label")
                .attr("style", "text-anchor: middle; font-weight: bold;")
                .text("% of Stack Overflow questions that month");
            // Setup colors for lines
            var color = d3.scaleOrdinal(d3.schemeCategory10);
            color.domain(data.map(function (c) { return c.tagName; }));
            // Function for getting the x/y values
            var valueline = d3.line()
                .x(function (d) { return x(d.yearMonth); })
                .y(function (d) { return y(d.percent); });
            // Draw the paths
            chartArea
                .append("g")
                .attr("class", "lines")
                .selectAll()
                .data(data)
                .enter()
                .append("path")
                .attr("class", "line")
                .attr("d", function (d) { return valueline(d.values); })
                .attr("data-legend", function (d) { return d.tagName; })
                .style("stroke", function (d) { return color(d.tagName); })
                .style("fill", "none")
                .on("mouseover", function (d) {
                d3.select(this)
                    .style("stroke-width", '4px');
                var selectlegend = $('.legend-items text').each(function (index, element) {
                    if ($(element).text() != d.tagName) {
                        $(element).css("opacity", .2);
                    }
                });
            })
                .on("mouseout", function (d) {
                d3.select(this)
                    .style("stroke-width", '1px');
                d3.selectAll('.legend-items text')
                    .style("opacity", 1);
            });
            // Draw legend
            var legendGroup = svg
                .append("g")
                .attr("class", "legend")
                .attr("transform", "translate(" + (width + margin.left + 20) + ", 30)")
                .style("font-size", "12px");
            d3.legend(legendGroup, "Tag");
            // Style lines/grid lines
            svg.selectAll(".grid-lines line")
                .attr("style", "stroke: #ccc; fill: none;");
            svg.selectAll(".tick text")
                .style("font-size", "12px");
        };
        Trends.prototype.updateDownloadLink = function () {
            var $trendChartDownloadLinkElement = $('#trend-chart-download-link');
            $trendChartDownloadLinkElement.attr('href', '');
            var html = d3.select("#trend-chart")
                .attr("title", "Stack Overflow Trends Chart")
                .attr("version", 1.1)
                .attr("xmlns", "http://www.w3.org/2000/svg");
            var $svg = $("#trend-chart");
            var svgHtml = $svg.parent().html();
            $trendChartDownloadLinkElement
                .attr("href-lang", "image/svg+xml")
                .attr("href", "data:image/svg+xml;base64,\n" + btoa(svgHtml));
        };
        return Trends;
    }());
    Insights.Trends = Trends;
    var ChartRenderer = (function () {
        function ChartRenderer() {
        }
        return ChartRenderer;
    }());
    Insights.ChartRenderer = ChartRenderer;
})(Insights || (Insights = {}));
$(function () {
    var trendsChart = new Insights.Trends();
});
//# sourceMappingURL=trends.js.map