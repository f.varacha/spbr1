[remark]:<class>(center, middle)
# Spring & Kafka

[remark]:<slide>(new)
## Co je to Kafka
[Apache Kafka®](https://kafka.apache.org/intro) je distribuovaná streamovací platforma. 

### Funkce Message broker:
- Kafku je mozné použít jako message Broker. 
  Tedy nástroj pro zasílání a příjem zpráv.
- Spring Boot podporuje kromě kafky:
  - **[JMS (Java Message Service)](https://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/jms.html)**:
    [ActiveMQ](https://activemq.apache.org/), [Artemis](https://activemq.apache.org/artemis/)
  - **[Advanced Message Queuing Protocol (AMQP)](https://www.amqp.org/)**: 
    [RabbitMG](https://www.rabbitmq.com/)
  
![Trendy podle StackOverFlow](media/stackoverflow.svg)
  
### Streaming 
- Komunikace založená na principu **publish-subscribe**
- Podporuje přehrávání starších zpráv.

### Robusnost
Kafka je vhodá pro systému s vysokou prostupnosti založených na replikaci nodu.
Samotná Kafka použí **[ZooKeeper](https://zookeeper.apache.org/)** pro vlastní replikace.


## Základní koncepty Kafky:
- Kafka je spuštěna jako klastr na jednom nebo více serverech, které mohou pokrývat více datových center.
- Klastr Kafky ukládá pracuje se *streamy* a záznamy (*records*) do kategorií zvaných *topic*.
- Každý **record se skládá z *key*, *value* a *timestamp*.

### Kafka má pět základních API:
- **[Producer API](https://kafka.apache.org/documentation.html#producerapi)**: publikace *records* k *topics*.
- **[Consumer API](https://kafka.apache.org/documentation.html#consumerapi)**: umožňuje přihlásit se k odběru jednoho nebo více *topic*.
- **[Streams API](https://kafka.apache.org/documentation/streams)**: umožňuje pracovat se streamy.
- **[Connector API](https://kafka.apache.org/documentation.html#connect)**: spojuje předchozí API (svým způsobem). Dá se napojit na například na databází, která pak publikuje každou změnu v tabuce.
- **[Admin API](https://kafka.apache.org/documentation.html#adminapi)**: umožňuje spravovat Kafku, práce s *topics*, manipulace s *brokers* atd.
 
## Doporučené UseCAses
K čemu se kafka hodí použít [^kafka_doc]:
- **Messaging**: Kafka lze použít stejne jako ActiveMQ or RabbitMQ.
- **Website Activity Tracking**: Toto je původní učel Kafky, tedy jako nástroje pro sledování aktivy uživatelů na webu.
- **Metrics**: Sběr provozních dat distribuovaných systému pro 
- **Log Aggregation**: Kafka je vhodná pro sběr logovacích dat a jejich agregci v centrální uložišti (např. [HDFS](https://hadoop.apache.org/)).
- **Stream Processing**: Například zpracování novinek, tweetu, RSS kanalá a vytváření real-time statistik a grafů.
- **Event Sourcing**: Podporuje tento aplikační design. Všechny události jsou uloženy spolu s časovou známkou a díky podpoře bigData je Kafka ideální pro tvorbu takto postavených aplikací.
- **Commit Log**: Kafka pomádá distribucu a replikaci dat mezu nody aplikace, podobně jako [Apache BookKeeper](https://bookkeeper.apache.org/).
 
[^kafka_doc]: [Podle dokumentace](https://kafka.apache.org/uses#uses_messaging)

# Instalace
Pokud chcem používat kafku můsíme mít:
- [ZooKeeper](https://zookeeper.apache.org/)
- [Kafka](https://kafka.apache.org/)

Pro vývoj je vhodnější mít všechno kontejnerizované pomoci [Docker](https://www.docker.com/) 

Pro tyto učely je k dipozici [docker-compose.yaml](docker-compose.yaml) v kořenovém adresáři.

Pro učely správy Kafky je možné použít nástroj [Kafkacat](https://github.com/edenhill/kafkacat), nebo nějakého ui Klienta, například [Kafka Tool](https://www.kafkatool.com/).

# Kafka Messaging
Nyní se podívejme na jednoduchý příklad posílání zpráv prostřednictví Kafky, jako jednoho ze základních UseCase scénářů:

## Nejpve vytvoříme *Topic* v Kafce.
K tomu můžeme využit primo script `kafka-topics.sh`, kafkacat příkaz nebo [Kafka Tool](https://www.kafkatool.com/) nebo přímo z Kodu.

```java
@Configuration
public class KafkaTopicConfig {

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        return new KafkaAdmin(singletonMap(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers));
    }

    @Bean
    public NewTopic springKafka() {
        return TopicBuilder.name("spring.kafka")
                .partitions(1)
                .replicas(1)
                .compact()
                .build();
    }
}
```

Pro puštění ještě potřebujeme konfiguraci v `application.yaml` souboru:
```yaml
spring:
  kafka:
    bootstrap-servers: localhost:9092
```

Vytvoření topicu můžeme zkontrolovat pomocí [Kafka Tool](https://www.kafkatool.com/).

## Message Porducent
Jako druhý krok vytvoříme producenta zpráv (*records*).

Jako zprávu můžeme použít `String` nebo **POJO** objekt, který použijeme pravě teď.

```java
public class Person {

    private UUID id = UUID.randomUUID();
    private String name;
    private int level;
    // constructor with name and level
    // getters and setters
    // hashcode a equeals
    // toString
}
```

Dále musíme nadefinovat klič a data zprávy:
- **Key**: Použijme UUID
- **Value**: Převedem object na JSON

Proto musíme buď vytvořit konfigurační class v Jave, ale v takto jednoduchém příkladě stačí konfigurace pomocí `application.yaml` souboru:
```yaml
spring:
  kafka:
    bootstrap-servers: localhost:9092
    producer:
      key-serializer: org.apache.kafka.common.serialization.UUIDSerializer
      value-serializer: org.springframework.kafka.support.serializer.JsonSerializer
```

Dále Doplním annotaci `@JsonIgnore` před `getId()` na třídě `Person`

A nakonec naimplementuje Service, která vude posílat zprávu o vytvoření osoby.
```java
@Service
public class PersonService {

    // @Value("${person.default-level}")
    private int defaultLevel = 1;

    // @Value("${person.kafka-topic}")
    private String springKafkaTopic = "spring.kafka";

    @Autowired
    private KafkaTemplate<UUID, Person> kafkaTemplate;

    public Person create(String name) {
        Person person = new Person(name, defaultLevel);
        kafkaTemplate.send(springKafkaTopic, person.getId(), person);
        return person;
    }
}
```


