package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private CommandLineRunner helloWorld;

	@Test
	void contextLoads() throws Exception {
		assertThat(helloWorld).isNotNull();
		helloWorld.run("");
	}

}
