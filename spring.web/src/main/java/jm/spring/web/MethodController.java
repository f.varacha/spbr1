package jm.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
class MethodController {

    // tag::requestParam[]
    @GetMapping("/request-param")
    public String requestParam(
            @RequestParam("petId") int petId,
            @RequestParam(name="own", required = false, defaultValue = "unknown") String name) {
        return "requestParam";
    }
    // end::requestParam[]
}

