= Spring: Web
Jan Kolomazník <jan.kolomaznik@gmail.com>
v1.0, 2020-07-03
include::../header.adoc[]


== Spring web - MVC
Spring Web MVC je původní webový framework postavený na Servlet API.
Název „Spring Web MVC“ pochází z původního názvu (spring-webmvc), ale je běžněji známý jako „Spring MVC“.

Spring obsahuje i další nástroje pro implementaci webové apilikace: „Spring WebFlux“.

=== Model - view - controle
Ppring pro implementací webu používá návrhový vzor link:https://cs.wikipedia.org/wiki/Model-view-controller[MCV].

image::media/mvc.png[MVC]

Každý webový požadavek zpracován Controllerem, což je třída zpravovanná Springema označená anotací `@Controller`.

Obsahují metody označené anotaci, která určuje adresu (může i více) a metodu pro kterou je tato metoda volaná.
Například `@RequestMapping(/hello)`, které odpovídají za zpracování požadavku metodou hello na adresy _/hello_.

Ukolem metod  Controlleru je připravit `model` and `view`:

- *`view`*: název pohledu uloženého v souboru: `src/main/resources/templates/{name}.html`
- může být vráceno jako `String` nebo součást objektu `ModelAndView`
- *`model`*: chytřejší mapa typu `<String, Object>`

=== Jednoduchý příklad: Implementace controleru

V následujícím příkladu `HelloController`:
- je zpracovává požadavky `GET /hello`

.HelloController.java
[source,java]
----
include::src/main/java/jm/spring/web/HelloController.java[tag=HelloController]
----
<1> Třída musí být označená annotaci `@Controller`
<2> Annotaci `@RequestMapping` definujeme mapování na *http request*.
<3> Spring mapuje *http request* hlavičku a další objekty na parametry této metody, viz dále.
<4> V tomto případě je návrotou hodnotou metody jméno šeblony, které se má vyrendrovat.

V předchozím příkladu metoda přijímá `Model` a vrací `název pohledu`, ale existuje mnoho dalších možností a jsou vysvětleny dále.

=== Dokončení příkladu: Vytvoření Šablony
pokud vychom chtěli dokončit MVC návrhový vzor, musíme ještě vytvořit čablonu.

Požadavek dále musí být zpracován šablonou, v tomto příkladu je použito Groovu template
.gradle.build
[source, groovy]
----
implementation 'org.springframework.boot:spring-boot-starter-groovy-templates'
----
Pro vytvoření jednoduché šablony, která já je umístěn v adresáři link:src/main/resources/templates/hello.html[resources/templates/hello.tpl].

.greeting.tpl
[source, groovy]
----
include::src/main/resources/templates/hello.html[]
----

výsledkem je html kód, který je zaslán klientovy.

Součástí stránek je často také statický obsah, (HTML, JavaScript, CSS, …) se umisťují do složky `src/main/resources/static`.

Jako statický zdroj je možné považovat například stránku `style.css`.


.style.css
[source, css]
----
include::src/main/resources/static/css/style.css[]
----

Pokud používáte Gradle, můžete spustit aplikaci pomocí `./gradlew bootRun` nebo pomocí IDE.

Poté můžete otestovat následující dva odkazy:

- link:http://localhost:8080/greeting[]
- link:http://localhost:8080/greeting?name=User[]

== Request Mapping
Pro mapovábí můžeme použít univerzální annotaci `@RequestMapping`.

* Tato annotace může být použíta pro mapovavání celého Controlleru.
* Můžete jej použít na vyjádření sdílených mapování nebo na úrovni metody k zúžení na konkrétní mapování koncového bodu.

Komě cesty a metody, jak bylo ukázáno v předchozím příkladu <<HelloController.java, HelloController>>.

Všehny parametry lze zapsah

. hodnotou: `value = "/hello"`
. výčtem: `value = {"/hello", "/hello.html"}`

Můžeme použít další parametry:

* *request parameters*:
** `params = "id"`
** `params = "id=1"`
* *headers*:
** `headers = { "key1=val1", "key2=val2" }`
* *media types*:
** `produces = "application/json"`
** `consumes = "application/json", "application/xml" }`

Spring rovněž děfinuje další annotace pro zkrácení zápisu:

* `@GetMapping`
* `@PostMapping`
* `@PutMapping`
* `@DeleteMapping`
* `@PatchMapping`

Existují ale i další annotace pro upřesnění práce s požadavkem:

* `@ResponseStatus(HttpStatus.CREATED)`: Která definuje response code metody, pokud nená vyvolána výjimka.
* `@ResponseBody`: Která říká, že návratový typ se vrací rovnou, venechá tedy rendrování šablony:

Příklad kombinací různých základních annotací:
.RequestMappingController.java
[source, java]
----
include::src/main/java/jm/spring/web/RequestMappingController.java[tag=RequestMappingController]
----

=== Mapovaní path
Nejdůležitějsím parametrem mapováví je cesta.

.multiplePaths
[source, java, indent=0]
----
include::src/main/java/jm/spring/web/MappingPathController.java[tag=multiplePaths]
----

==== Patterns
Cesta se rovněř může být definována pomocí specuálních znaků:

[cols="2,3,4"]
|===
| Vzor | Popisu vzoru | Příklad

| `?`
| Odpovídá jednomu znaku
| `"/pages/t?st.html"` odpovídá `"/pages/test.html"` a `"/pages/t3st.html"`

| `*`
| Odpovídá zádému nebo více znakům v segmentu cesty
| `"/resources/pass:[*].png"` odpovídá `"/resources/file.png"`

`"/projects/pass:[*]/versions"` odpovídá `"/projects/spring/versions"` ale neodpovídá `"/projects/spring/boot/version"`

| `**`
| Odpovídá žádnému nebo více segmentům cesty až do konce cesty
| `"/resources/**"` odpovídá `"/resources/file.png"` a `"/resources/images/file.png"`

| `{název}`
| Odpovídá segmentu cesty a zachycuje jej jako proměnnou s názvem `"name"`
| `"/projects/{project}/versions"` odpovídá `"/projects/spring/versions"` a zachycuje `project = spring`

| `{name: [a-z] +}`
| Shoduje se s regulárním výrazem "[a-z] +" jako proměnnou cesty s názvem "name"
| `"/projects/{project: [a-z] +}/versions"` shoduje s `"/projects/spring/versions"`, ale ne s `"/projects/ja-2020/version"`
|===

==== Path Variable
Pro mapování proměnných cesty se používá anotace `@PathVariable`.

.pathVariable
[source, java, indent=0]
----
include::src/main/java/jm/spring/web/MappingPathController.java[tag=pathVariable]
----

Typy na které lze proměnou mapovat jsou: `String`, `int`, `long`, `Date`, atd ...
S mapování mse dá pracovat pomoci `DataBinder`.

==== Spel
Brámci cesty mohou být také požívany plaseholdy `${...}`.
Toto se například hodí, když chcete mít stejný prefix pro určitou skupinu cest.

Následují příklad vytvoří mapoání na: `"/v2/owners"`.

.prefixSpel
[source, java, indent=0]
----
include::src/main/java/jm/spring/web/MappingPathController.java[tag=prefixSpel]
----

=== Media types
Dalším významým parametrem pro mapovaní je *media-type*.
Jedná se defakto o parametr http hlavičky, ale pro svoji důležitos je pro něj definovány dva specilní parametry annotace `@*Mapping`.
* `consumes`: definuje v jakém typu mohou data do tohoto endpointu přijít.
* `produces`: definuje jaké typy může metoda vyprodukoat.

NOTE::Nejčastější typy jsou definovány ve třídě `MediaType`.
Mezi nejpoužívanější patří: `APPLICATION_JSON_VALUE` a `APPLICATION_XML_VALUE`.

.produces
[source, java, indent=0]
----
include::src/main/java/jm/spring/web/MappingPathController.java[tag=produces]
----

Oba parametry také umí pracovat s negaci typu, tedy například `!text/plain` bude namapováno na všechny typy kromě uvedeného.

=== Parametry a headers
Při mapování paramtrů url path a parametrů hlaciřky je stejné.
Můžete použít jeden z následuících případe (nebo jejich kombinaci):

. Vyžadovat existenci parametru: `params = "myParam"`,
. nebo naopak neexistanci parametru: `params = "!myParam"`,
. nebo vyžadovat přesnou hodnotu: `params = "myParam=myValue"`.

=== Vlastní anotace
Spring MVC umožňuje ze základních anotaci odvozovat vlastní s přednastavenými nekterými hondotavmi, neboz skládající se z více anotace.

Dobrým případem jsou anotace `@GetMapping`, `@PostMapping`, ... které vznikly specializací annotace `@RequestMapping`.

== Zpracová metod
Kromě anntací je pracuje při mapování i se signaturou metody, u které anotace použíta.
Podstatnu roli hrají parametry, které jsou mapováné také.

Od verze hvy JDK 8 je rovněž možné pro parametry použít typ `Optional` a tím definovat, jestli jsou povinné nebo ne.

Takto je možné docílit stejného chování jako použítí anotace `@RequestParam`, `@RequestHeader` s parametrem `required=false`.

=== Argument metody
Spring prování mapování požadavku a další proměnných na argumenty metody.
Následující tabulka ukazuje několik nejazjímavějších typů:

.Argumenty metody
[cols="1,2"]
|===
| Argument metody | Popis

| `javax.servlet.ServletRequest`, `javax.servlet.ServletResponse`
| Vloží do metody objekt z `javax.servlet.api`

| `javax.servlet.http.HttpSession`
| Vynutí přítomnost relace.
  V důsledku toho tento argument není nikdy `null`.

  Všimněte si, že přístup k relaci není bezpečný pro vlákna.

| `java.security.Principal`
| Obsahuje informace o katálně přihlášeném uživateli.

| `HttpMethod`
| Metoda HTTP požadavku.

| `java.util.Locale`
| Aktuální národní prostředí požadavku.

| `java.io.InputStream`, `java.io.Reader`
| Pro přímé čtení z těla požadavku

| `java.io.OutputStream`, `java.io.Writer`
| Pro přístup k římému zápisu do odpovědi.

| `@PathVariable`
| Pro přístup k proměnným cesty

| `@MatrixVariable`
| Pro přístup k párům název-hodnota v segmentech cesty URI. Viz maticové proměnné.

| `@RequestParam`
| Pro přístup k parametrům požadavku, včetně vícedílných souborů.

  Hodnoty parametrů jsou převedeny na typ argumentu deklarované metody.

  *Použití `@RequestParam` je pro jednoduché hodnoty parametrů volitelné.*

| `@RequestHeader`
| Pro přístup k hlavičkám požadavků.

| `@CookieValue`
| Pro přístup k souborům cookie.

| `@RequestBody`
| Pro přístup k tělu požadavku HTTP.
  Obsah těla se převede na deklarovanou metodu typ argumentu pomocí implementací `HttpMessageConverter`.

| `java.util.Map`, `org.springframework.ui.Model`, `org.springframework.ui.ModelMap`
| Pro přístup k modelu, který se používá v řadičích HTML a je vystaven šablonám jako část vykreslování pohledu

| `RedirectAttributes`
| Určete atributy, které se mají použít v případě přesměrování.

| `Errors`, `BindingResult`
| Pro přístup k chybám z ověření a datové vazby při mapoavání pořasavku na argumenty.
|===

=== Návratové hodnoty
Nejen parametry mají specificý význam, návratový typ metody.
Následující tabulka ukazuje senzam teď nejzajímavějších.

.Return typy
[cols="1,2"]
|===
| Návratová hodnota metody | Popis

| `@ResponseBody`
| Vrácená hodnota je převedena prostřednictvím implementací HttpMessageConverter přímo do odpovědi.

| `HttpEntity<B>`, `ResponseEntity<B>`
| Definuje odpověď, včetně hlaviček a těla HTTP.

| `HttpHeaders`
| Pro vrácení odpovědi s hlavičkami a bez těla.

| `String`
| Název pohledu (šablony), která se má vyrendrovat.
  Za výběr šablony opovídá třída `ViewResolver`.

| `ModelAndView`
| Přepravka pro definci model u názvu pohledu.
  Použujete pokud nechcetem mít model v parametrech metody.

| `void`
| `void` nebo `null` znamená, že zpracována odpověď je kompletní, pokud má argumenty `ServletResponse`, `OutputStream` nebo
anotace `@ResponseStatus`.

V rámci REST rozhraní to také může znmenat "prázdnou odpověď"

| `Callable<V>`, `ListenableFuture<V>`, `CompletionStage<V>`, `CompletableFuture<V>`
| použije se v případě asynchronního zpracování požadavku.

| `StreamingResponseBody`
| Zápis do odpovědi OutputStream asynchronně.
|===

Jakákoli jiná návratová hodnota je buď poavažována za název pohledu.
Jednoduché typy jsou ignorovány.

=== Anotace @RequstParam
Anotací requst parama můžeme konfigurovat zpracování parametru.

Anotace umožňuje:
* předefinovat jméno parametru
* nastavit jeho povinnost. _Raději používejte `Optional`_.
* definovat výchozí hodnotu

.requestParam
[source, java, indent=0]
----
include::src/main/java/jm/spring/web/MethodController.java[tag=requestParam]
----

// https://docs.spring.io/spring-framework/docs/5.2.10.RELEASE/spring-framework-reference/web.html#mvc-ann-requestmapping
