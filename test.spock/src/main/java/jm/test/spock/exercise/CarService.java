package jm.test.spock.exercise;

public interface CarService {

    Car createCar(String name);

    Car rentCar(Car car, String user);

    int createId(String name);
}
